var app = angular.module('app', []);

app.controller("postController" , ["$scope" , "$http", function($scope, $http){
  $scope.posts = [];
  $scope.post;

  $scope.addPost = function(){
    $http.post("/api/post", $scope.post)
    .then(function(response){
      console.log(response.data);
      $scope.post = "";
      $scope.posts.push(response.data.result)
    });
  }

  $scope.upvotePost = function(id){
    $http.put("/api/post/vote/"+ id)
    .then(function(response){
      alert("Post Updated");
    });
  }

  $http.get("/api/post")
  .then(function(response){
    $scope.posts = response.data.result;
  });
}]);
