/**
 * User: Himanshu Jain
 * Date: 5/18/17
 * Time: 11:16 AM
 */

//Utility to make calls to API server and prepare request for calls etc
//

'use strict';

/**
 * makes http call to server and also handles
 * @param options - configuration and data for call
 * @param cb - callback method to be executed when response is received
 */
exports.replaceString  = function (templateString, obj) {

  var generatedString = templateString.replace(/__(\w+)__/g, function(_,k){
    return obj[k] || " ";
  });
  return generatedString;
}
