/**
 * User: himanshujain.2792
 * Date: 5/18/17
 * Time: 11:16 AM
 */

const mongoose = require('mongoose');
/**
 * Connect to MongoDB.
 */


function onConnectionError (err) {
    console.log("Mongoose connection error"+err);
}

function onConnect () {
    console.log("Mongoose connection open to");
}

function onDisconnect () {
    console.log("Mongoose connection disconnected");
}

function registerEvents () {
    mongoose.connection.on("connected", onConnect);

    // If the connection throws an error
    mongoose.connection.on("error", onConnectionError);

    // When the connection is disconnected
    mongoose.connection.on("disconnected", onDisconnect);

    // If the Node process ends, close the Mongoose connection
    process.on("SIGINT", function () {
        mongoose.connection.close(function() {
            console.log("warn", "Mongoose connection disconnected through app termination");
            process.exit(0);
        });
    });
}

mongoose.Promise = Promise;

module.exports = function(app){
    registerEvents();
    conn =  mongoose.connect(process.env.MONGODB_URI , { useMongoClient: true, /* other options */ })

    // conn = mongoose.createConnection(process.env.MONGODB_URI || process.env.MONGOLAB_URI);    
};


