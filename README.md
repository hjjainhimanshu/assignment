# himanshujain.2792 Boilerplate

### Version
0.0.1

This Product uses some open source packages:

* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework


### Installation

this requires [Node.js](https://nodejs.org/) v4.4+ to run.


Open your favorite Terminal and run these commands.

### Development

Want to contribute? Great!

First Tab:
```sh
$ node app
```

**Awesome stuff, Hell Yeah!**


   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
