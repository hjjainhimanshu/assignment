/**
 * User: himanshujain.2792
 * Date: 5/18/17
 * Time: 11:16 AM
 */

'use strict';

const express = require('express');
const router = express.Router();
const homeController = require('../controllers/homeController');
/**
 * GET '/'
 * Default home route. Just relays a success message back.
 * @param  {Object} req
 * @return {Object} json
 */
router.get('/', homeController.index);
router.post('/api/post', homeController.addPost);
router.get('/api/post', homeController.getPosts);
router.put('/api/post/vote/:id', homeController.upvotePost);
//router.get('/*', errorController._404);


module.exports = router;
