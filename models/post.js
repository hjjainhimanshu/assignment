var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
    content : String,
    upvotesCount: {type:Number,default:0},
}, {strict: false, collection: 'post', timestamps: true});


var postModel = conn.model('post', postSchema);


module.exports = postModel;