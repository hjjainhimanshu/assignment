/**
 * GET /
 * Home page.
 */

'use strict';
let rp = require("request-promise");
let postModel = require("../models/post");

exports.index = function(req, res) {
  res.sendfile('./public/index.html');
};


exports.addPost = function(req, res) {
    postModel.create(req.body).then(function(data) {
        return res.json({'message': 'Added Post', 'result': data, 'error': false});
    }).catch(function(err)  {
        console.log(err);
        return res.json({'message': 'Server Error', 'result': [], 'error': true}).status(500);
    });
};

exports.getPosts = function(req, res) {
    postModel.find().then(function(data) {
        return res.json({'message': 'Posts', 'result': data, 'error': false});
    }).catch(function(err)  {
        console.log(err);
        return res.json({'message': 'Server Error', 'result': [], 'error': true}).status(500);
    });
};


exports.upvotePost = function(req, res) {
    console.log(req.params.id);
    postModel.findByIdAndUpdate(req.params.id, { $inc: { upvotesCount: 1}}).then(function(data) {
        return res.json({'message': 'Updated Post', 'result': data, 'error': false});
    }).catch(function(err)  {
        console.log(err);
        return res.json({'message': 'Server Error', 'result': [], 'error': true}).status(500);
    });
};