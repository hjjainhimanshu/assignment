/**
 * Module dependencies.
 * User: himanshujain.2792
 * Date: 5/18/17
 * Time: 11:16 AM
 */

"use strict";

let express = require('express');
let compression = require('compression');
let bodyParser = require('body-parser');
let logger = require('morgan');
let cors = require('cors');
let path = require('path');
let errorHandler = require('errorhandler');
let dotenv = require('dotenv');
let expressValidator = require('express-validator');
let errorController = require('./controllers/errorController');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */




/**
 * Create Express server.
 */
let app = express();
dotenv.load({ path: ".env" });



require('./config/db')(app);
/**
 * development error handler
 *
 * will print stacktrace
 */
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: true,
      data: { error: err }
    });
  });
}

/**
 * production error handler
 *
 * no stacktraces leaked to user
 */
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: true,
      data: { }
  });
});

/**
 * Express configuration.
 */
app.use(cors());
app.set('port', process.env.PORT || 3000);
app.set('json spaces', 4);
app.set('x-powered-by', false);
app.set('view options', {pretty:true});
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public"))); // serve static files before auth
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

/**
 * Error Handler.
 */
if (app.get('env') === 'dev') {
  app.use(errorHandler());
}

/**
 * our routes will be contained in routes/indexRoutes.js
 */
var routes = require('./routes/indexRoutes');
app.use('/', routes);


/**
 * catch 404 and forward to error handler
 */

app.all('*', errorController._404);
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Start Express server.
 */
app.listen(app.get('port'), function() {
  console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;
